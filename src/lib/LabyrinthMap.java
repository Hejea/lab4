package lib;

import java.awt.Point;

public class LabyrinthMap implements LabyrinthModel {
	public int[][] map;
	public Point start;
	public Point finis;
	
	public LabyrinthMap() {
		super();
		this.map = new int[20][20];
		this.start = new Point(0, 0);
		this.finis = new Point(0, 0);
	}
	public LabyrinthMap(int[][] map, Point start, Point finis) {
		super();
		this.map = new int[map.length][];
		for(int i = 0; i < map.length; i++)
			this.map[i] = map[i].clone();
		this.start = new Point(start);
		this.finis = new Point(finis);
	}
	public int getRowCount() {
		return map.length;
	};
	public int getColumnCount() {
		return map[0].length;	
	};
	public Boolean isFreeAt(int x, int y) {
		if(!(map[x][y] == 1)) return true;
		return false;
	};
	public Boolean isWallAt(int x, int y) {
		if(map[x][y] == 1) return true;
		return false;
	};
	public Point getStartCell() {
		return this.start;
	};
	public Point getFinishCell() {
		return this.finis;
	};
}
