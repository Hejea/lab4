package lib;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class LabyrunthDiscover implements LabyrinthSolver {

	@Override
	public List<List<Point>> solveManual(LabyrinthMap map) {
		List<List<Point>> path = new ArrayList<List<Point>>();
		
		
		return path;
	}

	@Override
	public List<List<Point>> solveAutomat(LabyrinthMap map) {
		Dfs rez = new Dfs(map.map, map.start, map.finis);
		return rez.Exploreaza();
	}
	
	
	private static class Dfs {
		public Point s, f;
		public int[][] y;
		public List<List<Point>> list;
		public final int[] dx = {-1,0,1,0};
		public final int[] dy = {0,1,0,-1};
		public int[][] a;

		public Dfs(int[][] a, Point s, Point f) {
			super();
			this.a = a;
			this.y = new int[a.length*a.length][2];
			this.s = s;
			this.f = f;
			this.list = new ArrayList<List<Point>>();
		}

		public int posibil(int k) {
// verificam daca sunt coordonate posibile
			if( (y[k][0] < 0) || (y[k][0] >= this.a.length) ||
		        (y[k][1] < 0) || (y[k][1] >= this.a[0].length) ) return 0;
			
// verificam daca in casuta este 0 sau 1
		    if( this.a[y[k][0]][y[k][1]] == 1) return 0;    
		    
// verificam daca casuta curenta este o casuta vizitata anterior
		    for(int l = 0; l < k; l++)
		        if( (y[k][0] == y[l][0]) && (y[k][1] == y[l][1]) ) return 0;    
		    return 1;
		}
// afisam la ecran solutia		
		public void scrie(int k)           
		{
		    List<Point> aux = new ArrayList<Point>();
		    for(int i = 0; i <= k; i++) {
		        aux.add(new Point(y[i][0], y[i][1]));      
		    }
		    this.list.add(aux);
		}
// aflam toate drumurile posibile pina la prima iesire 
		public void DrumP(int k)                        
		{
		    for(int l = 0; l < 4; l++)
		    {
		        this.y[k][0] = y[k-1][0] + dx[l];
		        this.y[k][1] = y[k-1][1] + dy[l];
		        if(posibil(k) == 1)
		        {
		            if(y[k][0] == f.x && y[k][1] == f.y) {
		                scrie(k);
		            }
		            else DrumP(k+1);
		        }
		    }
		}
		
		public List<List<Point>> Exploreaza() {
			this.y[0][0] = s.x;
			this.y[0][1] = s.y;
    		DrumP(1);
			return list;                          
		}
	}
}
