package lib;

import java.awt.Point;

public interface LabyrinthModel {
	public int getRowCount();
	public int getColumnCount();
	public Boolean isFreeAt(int x, int y);
	public Boolean isWallAt(int x, int y);
	public Point getStartCell();
	public Point getFinishCell();
}
