package lib;

public interface LabyrinthView {
	public void toConsoleMatrix(LabyrinthMap lMap);
	public void toConsoleChamber(LabyrinthMap lMap);
}
