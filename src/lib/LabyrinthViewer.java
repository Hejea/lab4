package lib;

import java.awt.Point;
import java.util.List;

public class LabyrinthViewer implements LabyrinthView {
	public void toConsoleMatrix(LabyrinthMap lMap) {
		for(int i = 0, j; i < lMap.map.length; i++) {
			for(j = 0; j < lMap.map[i].length; j++) {
				System.out.printf("%2d", lMap.map[i][j]);
			}
			System.out.println();
		}
	};
	public void toConsoleChamber(LabyrinthMap lMap, String wall, String free, String start, 
			 String finis, Boolean border) {
		String s = "+";
		if(border) {
			for(int i = 0; i < 2*lMap.map.length + 1; i++) s += "-"; s += "+";
			System.out.println(s);
		}
		for(int i = 0, j; i < lMap.map.length; i++) {
			if(border) System.out.print("|");
			for(j = 0; j < lMap.map[i].length; j++) {
				if(lMap.map[i][j] == 0)
					System.out.printf("%2s", free);
				else if(lMap.map[i][j] == 1)
					System.out.printf("%2s", wall);
				else if(i == lMap.start.x && j == lMap.start.y) 
					System.out.printf("%2s", start);
				else if(i == lMap.finis.x && j == lMap.finis.y)
					System.out.printf("%2s", finis);
			}
			if(border) System.out.print(" |");
			System.out.println();
		}
		if(border) System.out.println(s);
	}
	public void toConsoleChamber(LabyrinthMap lMap) {
	/*     +-----------+
	 *	   | S # # # # |
	 *	   | . . # # # |
	 *	   | # . # # # |
	 *	   | # . . # # |
	 *	   | # # F # # |
	 *	   +-----------+
	 */
		this.toConsoleChamber(lMap, "#", ".", "S", "F", true);
	}
	public void toConsolePath(List<List<Point>> list) {
		int[] dx    = {      0,      1,      0,   -1};
		int[] dy    = {      1,      0,     -1,    0};
		String[] ds = {"Right", "Down", "Left", "Up"};
		
		for (int i = 0, j, x, y, px, py, k; i < list.size(); i++) {
			System.out.format("\n%7s : %d\n", "Drumul", i + 1);
			for(j = 0; j < list.get(i).size(); j++) {
				x = list.get(i).get(j).x;
				y = list.get(i).get(j).y;				
				if(j == 0) {
					System.out.format("%7s - %2d, %2d\n", "Start", x, y);
				} else {
					px = list.get(i).get(j-1).x;
					py = list.get(i).get(j-1).y;
					for(k = 0; k < 4; k++) {
						if(px == x + dx[k] && py == y + dy[k]) {
							System.out.format("%7s - %2d, %2d\n", ds[k], x, y);
						}
					}
				}
			}
			System.out.format("%7s\n", "Finish");
		}
	}
	
}

