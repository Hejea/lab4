package lib;

import java.awt.Point;
import java.util.List;

public interface LabyrinthSolver {
	public List<List<Point>> solveManual(LabyrinthMap map);
	public List<List<Point>> solveAutomat(LabyrinthMap map);
}