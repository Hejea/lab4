package lib;

import java.awt.Point;

public class Test {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		int[][] m1 = {  {-1, 1, 1, 1, 1},
						{ 0, 0, 1, 1, 1},
						{ 1, 0, 1, 1, 1},
						{ 1, 0, 0, 1, 1},
						{ 1, 1, 2, 1, 1}};
		int[][] m2 = {  {-1, 0, 0, 0, 1},
						{ 0, 1, 1, 0, 1},
						{ 0, 0, 0, 0, 0},
						{ 0, 1, 0, 1, 0},
						{ 0, 1, 2, 1, 0}};
		
		LabyrinthViewer view = new LabyrinthViewer();
		LabyrunthDiscover explr = new LabyrunthDiscover();
		LabyrinthMap lm1 = new LabyrinthMap(m1, new Point(0, 0), new Point(4, 2));
		LabyrinthMap lm2 = new LabyrinthMap(m2, new Point(0, 0), new Point(4, 2));
		
		view.toConsoleMatrix(lm2);
		view.toConsoleChamber(lm2);
		view.toConsolePath(explr.solveAutomat(lm2));
	
	}
}
